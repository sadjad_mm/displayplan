@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Users List') }}</div>

                <div class="card-body">

                    @foreach ($users as $user)
                        <div>
                            <a href="/profile/{{$user->username}}"> {{$user->username}}</a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
