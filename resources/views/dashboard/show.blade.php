@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Profile') }}</div>

                <div class="card-body">

                    <div class="row mb-3">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                        {{ $user->name }}
                    </div>

                    <div class="row mb-3">
                        <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>
                        {{ $user->phone }}
                    </div>

                    <div class="row mb-3">
                        <label for="address" class="col-md-4 col-form-label text-md-right">{{ __('Address') }}</label>
                        {{ $user->address }}
                        
                    </div>

                    <div class="row mb-3">
                        <label for="sex" class="col-md-4 col-form-label text-md-right">{{ __('Sex') }}</label>
                        {{$user->sex}}
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
