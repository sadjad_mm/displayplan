<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the user dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function profile()
    {
        $user = auth()->user();
        return view('dashboard.index', ['user'=>$user]);
    }

    /**
     * Update the user info.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function updateProfile(Request $request)
    {
        $data = $request->validate([
            'name' => ['required', 'string', 'min:2' , 'max:25'],
            'phone' => ['string', 'max:11'],
            'address' => ['string', 'min:3'],
            'sex' => ['string', 'in:Male,Female'],
        ]);

        $user = auth()->user();

        $update = $user->update([
            'name' => $data['name'],
            'phone' => $data['phone'],
            'address' => $data['address'],
            'sex' => $data['sex']
        ]);
        return redirect('/profile');
    }


    /**
     * show a user's profile.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showProfile(Request $request, $username)
    {

        if (isset($username) && !is_null($username)) {
            $user = User::where('username', '=', $username)->first();
        }

        if ($user) {
            return view('dashboard.show', ['user'=>$user]);
        }
    }

    /**
     * show users list.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function users()
    {

       $users = User::get();
       return view('dashboard.users', ['users'=>$users]);

    }

}
